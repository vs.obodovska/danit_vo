/// Теоретичні питання

// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Відповідь: екранування потрібне для того, щоб записати в стрінгу спеціальні символи як "" (для цитат наприклад - \"), 
// чи ' (для апострофа - \').  

// Які засоби оголошення функцій ви знаєте?
// Відповідь: function declaration statement - оголошення функції (вказується назва функції (її параметри) і тіло), 
// function definition  expression - через запис у змінну (і може бути як іменована, так і ні), або Arrow Function - 
// стрілковий запис функції (також може бути методом в Об'єкті).

// Що таке hoisting, як він працює для змінних та функцій?
// Відповідь: підняття функції чи оголошення змінної на початку кода. Функцію наприклад можна записати до оголошення 
// змінних, які в ній використовуються. Щодо ж до оголешення змінних, то при роботі з ними можуть бути проблеми, 
// якщо їх оголосити не зверху перед якоюсь дією з нею, а після дії.


/// Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.


// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) 
// і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) 
// і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
// з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function readFirstName() {
    return prompt("Please enter your first name.");
  }

function readLastName() {
    return prompt("Please enter your last name.");
  }

  function readBirthDay() {
    return prompt("Please enter your birth date", "dd.mm.yyyy");
  }
  
function createNewUser() {
    return {
      firstName: readFirstName(),
      lastName: readLastName(),
      birthDay: readBirthDay(),
      getLogin() {
        return (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
      },
      getAge() {
            let age = new Date().getFullYear() - this.birthDay.slice(6);
            return age;
      },
      getPassword() {
          return this.firstName.charAt(0).toUpperCase(0) + this.lastName.toLowerCase() + this.birthDay.slice(6);
      }
    }
  }

  let newUser = createNewUser();
  console.log(newUser);
  console.log(newUser.getAge());
  console.log(newUser.getPassword());