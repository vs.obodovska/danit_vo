/// Теоретичні питання

// Опишіть своїми словами що таке Document Object Model (DOM)
//Відповідь: об'єктно структурована модель документа, де кожен об'єкт може бути змінений.
// В js можна маніпулювати та змінювати контент і події для кожного обєктного елементу DOM-дерева.

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//Відповідь: innerHTML - повертає в текстовому вигляді з внутрішніми тегами або вставляє і парсить як тег з текстом,  
// innerText - внутрішні теги ігноруються і виводиться/ змінюється лише текст

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//Відповідь: 1) getElement(s)By__по його ID (лише один елемент), колекцію через: Name, TagName, ClassName - усі вони повертають NodeList, 
// який є живим, якого можна змінювати і впливат на нього;
// або 2) через css селектори querySelector (повертає перший отриманий,що підходить) querySelectorAll (колекцію у вигляді NodeList (і має довжину), 
// з якими можна далі працювати і використовувати інші методи і цикли (for чи for of) або перетворити в Array і використати for.Each).
// ще 3) можна використати closest(), яле він пошук веде вгору, аніж querySelector, який шукає далі вниз по елементам.


/// Завдання

// Код для завдань лежить в папці project.
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphElements = document.getElementsByTagName("p");
for (const element of paragraphElements) {
    element.style.backgroundColor = "#ff0000";
}

// Знайти елемент із id="optionsList". Вивести у консоль. 
const optionsListElement = document.getElementById('optionsList');
console.log(optionsListElement);
// Знайти батьківський елемент та вивести в консоль. 
const optionsListParent = optionsListElement.parentElement;
console.log(optionsListParent);

// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
for (const element of optionsListElement.childNodes) {
    console.log(element.nodeName, element.nodeType);
  } 

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
const testElement = document.getElementById('testParagraph');
testElement.innerText = "This is a paragraph";
// This is a paragraph

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
 const mainHeaderElement = document.querySelector('.main-header');
 for (const element of mainHeaderElement.childNodes) {
    console.log(element);
  }

console.log(mainHeaderElement.children);
 
// Кожному з елементів присвоїти новий клас nav-item.
 const childOfMainHeaderElement = mainHeaderElement.children;
 for (let i = 0; i<childOfMainHeaderElement.length; i++ )  {
    childOfMainHeaderElement[i].className = 'nav-item'; ////присвоїть замість існуючих цей
 }
// або
//  for (const element of childOfMainHeaderElement) {
//     element.className = 'nav-item'; //присвоїть замість існуючих цей
//     // element.className += ' nav-item'; //додасть до існуючих ще один
//   }
// for (const element of childOfMainHeaderElement) {
//     element.classList.add('nav-item'); //додасть до існуючих ще один
//   }

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const removedClassElement = document.querySelectorAll('section-title');
for (const element of removedClassElement) {
    element.remove('section-title');
}



