// Теоретичні питання

// Описати своїми словами навіщо потрібні функції у програмуванні.
// Відповідь: для оптимізації дій під час створення програми. Якщо одну й ту саму дію потрібно
// в подальшому виконувати більш, ніж 1 раз, то краще написати функцію код і потім викликати лише її
// в тому чи іншому місці і не дублювати увесь код. функція, є складовою частиною будь-якого програмування.
// Функція може приймати значення і повертати результат її виконання безліч разів.

// Описати своїми словами, навіщо у функцію передавати аргумент.
// Відповідь: для того, щоб функція розуміла, які параметри їй використовувати для повернення результату її виконання.
// В той же час, потрібно зважати на кількість праметрів аргументу, адже відсутність хоча б одного з них при введенні може призвезти
// до того, що фугкція працюватиме криво - буде або undefined або NaN, тощо.

// Що таке оператор return та як він працює всередині функції?
// Відповідь: це оператор, який повертає задані йому значення/дані, або undefined, якщо значення пусте в самому return і в аргементі функції; 
// також може використовуватись для переривання виконання функції у точці, де вказуєтся return і виходу з функції та/або виведення результату
// виконання функції (її частини) типу true або false.

// Завдання

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
// Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або
// при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для
// кожної зі змінних має бути введена інформація раніше).

const supportedOperators = ["+", "-", "*", "/"];


function isValidNumber(number) {
  return !Number.isNaN(number);
}

function readNumber() {
  return +prompt("Please, provide number", "123...");
}

function readValidNumber() {
  let validNumber;
  do {
    validNumber = readNumber();
  } while (!isValidNumber(validNumber));
  return validNumber;
}

function isValidOperator(operator) {
  return supportedOperators.includes(operator);
}

function readOperator() {
  return prompt(`Please, provide an operator ${supportedOperators.join(",")}.`);
}

function readValidOperator() {
  let validOperator;
  do {
    validOperator = readOperator();
  } while (!isValidOperator(validOperator));
  return validOperator;
}

function executeOperationString(numberOne, numberTwo, operator) {
    return  `Executing operation ${numberOne} ${operator} ${numberTwo} = `;
}

function executeOperation(numberOne, numberTwo, operator) {
  switch (operator) {
    case "+":
      return numberOne + numberTwo;
    case "-":
      return numberOne - numberTwo;
    case "*":
      return numberOne * numberTwo;
    case "/":
      return numberOne / numberTwo;
      default:
          console.log(`Operator ${operator} is not supported`);
          return false;
  }
}

let numberOne = readValidNumber();
let numberTwo = readValidNumber();
let operator = readValidOperator();
let operationString = executeOperationString(numberOne, numberTwo, operator);
let result = executeOperation(numberOne, numberTwo, operator);

console.log(operationString + result);
