
const darkThemeStyle = {
    main: '#000',
    submitText: '#222527',
    };

const lightThemeStyle = {
    main: '#FFFFFF',
    submitText: '#FFFFFF',
}
const lightTheme = 'light';
const darkTheme = 'dark';

let currentThemeKey = "currentTheme";

const element = document.documentElement;

function switchTheme () {
    const signUpContainer = document.querySelector('.signup');
    let currentTheme = readLocalStorageTheme();
    if (currentTheme === lightTheme) {
        setTheme(element, darkThemeStyle);
        signUpContainer.classList.add('signup--dark');
        currentTheme = darkTheme;
    } else {
        setTheme(element, lightThemeStyle);
        signUpContainer.classList.remove('signup--dark');
        currentTheme = lightTheme;
    }
    localStorage.setItem(currentThemeKey, currentTheme)
}

function  readLocalStorageTheme(){
    const theme = localStorage.getItem(currentThemeKey);
    return theme === null? lightTheme: theme;
}
function  setTheme(element, theme){
    element.style.setProperty('--theme-main', theme.main);
    element.style.setProperty('--theme-submit-text', theme.submitText);
}

const themeSwitch = document.querySelector('.switcher-theme-name');

themeSwitch.addEventListener('click', switchTheme);
window.onload = () => {
    const signUpContainer = document.querySelector('.signup');
    let currentTheme = readLocalStorageTheme();
    if (currentTheme === lightTheme) {
        setTheme(element, lightTheme);
        signUpContainer.classList.remove('signup--dark');
    } else {
        setTheme(element, darkThemeStyle);
        signUpContainer.classList.add('signup--dark');
    }
}