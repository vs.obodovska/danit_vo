// Теоретичні питання

// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Відповідь: для виконання визначеної чи не визначеної кількості однотипних задач шляхом викладення такої дії в одному записі,
// що зменшує кількість копіювання однотипних дій в самому коді.

// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// Відповідь:
// 1. цикл "for" для того випадку, коли розуміємо чітку послідовність/кількість виконання циклу/ітерацій, а також за
// його допомогою можливо інціалізувати нову змінну для роботи з нею в середині циклу;
// 2. цикл "while" - має лише умову циклу, на відміну від for. Виконується до тих пір, поки результат циклу буде true.
// І не виконається жодного разу, якщо при вході умова буде false.
// 3. цикл "do"-"while" - виконується принаймні раз при вході в блок кода do, навіть якщо умова false.

// Що таке явне та неявне приведення (перетворення) типів даних у JS?
// Відповідь: явне приведення, це коли навмисно і чітко вказуєтся перетворення одразу - наприклад приведення до Number('123'), або String(123)
// Неявне ж перетворення - може виконуватись завдяки виконанню певних операцій  логічних, арифметичних, тощо. Наприклад == викликає неаявне перетворення типів, якщо це потрібно, а + може склеювати і виконувати неявне перетворення як, наприклад під час виконання операції: +'123'.

// Завдання

// Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без залишку) у
//заданому діапазоні. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль
// фразу "Sorry, no numbers"
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
//var1
let userNumber1 = +prompt("Please, provide Number 1");
if (userNumber1 < 0) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNumber1; i = i + 5) {
    console.log(i);
  }
}
// //var2
let userNumber2 = +prompt("Please, provide Number 2");
if (userNumber2 < 0) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNumber2; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}

// Необов'язкове завдання підвищеної складності
// Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран,
// доки не буде введено ціле число.
// Отримати два числа, m і n. Вивести в консоль усі прості числа
// в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел не відповідає
// умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.

function isValidNumber(number) {
  return Number.isInteger(number);
}
function readIntegerNumber() {
  return +prompt("Please, provide Integer number");
}
function readValidIntegerNumber() {
  let validNumber;
  do {
    validNumber = readIntegerNumber();
  } while (!isValidNumber(validNumber));
  return validNumber;
}
function checkThatNumbersCorrect(minNumber, maxNumber) {
  return minNumber >= 0 && maxNumber > 1 && maxNumber > minNumber;
}
let m = readValidIntegerNumber();
let n = readValidIntegerNumber();


let lowerNumber = Math.min(m, n);
let higherNumber = Math.max(m, n);

while (!checkThatNumbersCorrect(m, n)) {
    alert("Please enter correct range.")
    m = readValidIntegerNumber();
    n = readValidIntegerNumber();
    lowerNumber = Math.min(m, n);
    higherNumber = Math.max(m, n);
}
let isPrimeNumberFound = false
for (let i = lowerNumber; i <= higherNumber; i++) {
    let flag = 0;
    for (let j = 2; j < i; j++) {
      if (i % j == 0) {
          flag = 1;
          break;
      }
  }
  if (i > 1 && flag == 0) {
    isPrimeNumberFound = true
    console.log(i);
}
}
if (!isPrimeNumberFound)
console.log(`No prime numbers found in range ${lowerNumber}..${higherNumber}`)
