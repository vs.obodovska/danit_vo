/// Теоретичні питання

// Опишіть, як можна створити новий HTML тег на сторінці.
// Відповідь: потрібно використати метод createElement(), задати йому className, наповнити змістом (наприклад через innerHTML), а далі розмістити його за допомогою методів: after(), before(), append(), prepend(), а також через insertAdjacentHTML("afterBegin"/"afterEnd"/"beforeBegin"/"beforeEnd", ` `)

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Відповідь: це розміщення нового елементу відносно того елементу, до якого був викликаний даний метод.
// "afterBegin" - одразу після відкриваючого тега елемента, до якого дадається новий елемент,
//  "afterEnd" - одразу після закриваючого  тега елемента, до якого дадається новий елемент,
// "beforeBegin" - перед елементом, до якого дадається новий елемент/ до його відкриваючого тега
// "beforeEnd" - перед закриваючим тегом елемента, до якого дадається новий елемент/ в кінці усіх його дітей

// Як можна видалити елемент зі сторінки?
// Відповідь: використати метод remove()


/// Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Варіант 1://
// const arr1 = ["hello", "world", "Kyivv", "Kharkiv", "Odesa", "Lviv"];
// const arr2 = ["1", "2", "3", "sea", "user", 23];
// const arr3 = ["Kharkiv", "Kyiv", ["Boryspil", "Irpin"], "Odesa", "Lviv", "Dnipro"].flat();
// arr1.forEach(element => showElements(element));
// arr2.forEach(element => showElements(element));
// arr3.forEach(element => showElements(element));

// function showElements(element) {
//     const listElement = document.createElement("ul");
//     listElement.insertAdjacentHTML('afterbegin', `<li>${element}</li>`);
//     listElement.className = "list";
//     document.body.before(listElement);
//     // setTimeout(() => listElement.remove(), 3000);
// }

// Варіант 2://
const arr1 = ["hello", "world", "Kyivv", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const arr3 = ["Kharkiv", "Kyiv", ["Borispol", "Irpin"], "Odesa", "Lviv", "Dnipro"].flat();

const parentElement = document.querySelector('body');

// showElements (arr2, parentElement);

function showElements (arr, parent) {
    arr.forEach(element => {
    const listElement = document.createElement("ul");
    listElement.insertAdjacentHTML('afterbegin', `<li>${element}</li>`);
    listElement.className = "list";
    parent.append(listElement);
    setTimeout(() => listElement.remove(), 3000);
})
}

// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.

// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:
// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
const revisedArr3 = arr3.map(function(elem){
    return elem;
});
showElements (revisedArr3, parentElement);

// Очистити сторінку через 3 секунди. /// РЕАЛІЗОВАНО В ФУНКЦІЇ ВИЩЕ!
// Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
window.onload = function() {
    var minute = 0;
    var sec = 2;
    setInterval(function() {
      document.getElementById("timer").innerHTML = ` 0${minute}  : 0${sec}`;
      if (sec > 0) {
        sec--
      }
      else if (sec == 0) {
          sec
      }
    }, 1000);
  }