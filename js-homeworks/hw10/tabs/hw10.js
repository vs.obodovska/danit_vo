const tabsTitleElements = document.querySelectorAll(".tabs-title");
tabsTitleElements.forEach((tab) =>
  tab.addEventListener("click", () => {
    const attr = tab.dataset.tab;
    document.querySelector(".tabs-title.active").classList.remove("active");
    tab.classList.add("active");
    document.querySelector(".tab-content.active").classList.remove("active");
    document
      .querySelector(`li[data-content="${attr}"]`)
      .classList.add("active");
  })
);
