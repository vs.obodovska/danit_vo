var activeIndex = 0;
var startPosition = 0;
const limitToShow = 4;

class User {
  constructor(name, position, feedback, photoName) {
    this.name = name;
    this.position = position;
    this.feedback = feedback;
    this.photoSrc = "./img/ClientFeedback/" + photoName;
  }
}

const usersInfo = [
  new User("Hasan Ali", "UX Designer", "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.", "Layer 5.png"),
  new User("Marge Simpson", "Web Designer", "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Recusandae quae vitae velit ipsa doloribus in, magnam inventore unde veritatis voluptatibus fugit perferendis totam nam ducimus, illo perspiciatis. Ex, culpa saepe?", "Layer 6.png"),
  new User("Bart Simpson", "CEO", "Everything is good", "Layer 7.png"),
  new User("Maggie Simpson", "Student", "It's greate job", "Layer 8.png"),
  new User("Marge 1 Simpson", "Graphic Designer", "Very good team", "Layer 9.png"),
  new User("Bart 1 Simpson", "Client", "Helpful services", "Layer 10.png"),
];
function prepareListToPrint() {
  const endPosition =
    startPosition + limitToShow < usersInfo.length
      ? startPosition + limitToShow - 1
      : usersInfo.length - 1;
  const listToPrint = [];
  for (let i = startPosition; i <= endPosition; i++) {
    listToPrint.push(usersInfo[i]);
  }
  if (
    endPosition - startPosition < limitToShow - 1 &&
    usersInfo.length > limitToShow
  ) {
    const endPosition2 = limitToShow - listToPrint.length;
    for (let i = 0; i < endPosition2; i++) {
      listToPrint.push(usersInfo[i]);
    }
  }
  return listToPrint;
}
function drawPictures() {
  const usersToPrint = prepareListToPrint();
  const elementToAppendPhotos = getUserPhotosHTMLElement();
  var htmlElementsToOutput = "";
  for (let i = 0; i < usersToPrint.length; i++) {
    htmlElementsToOutput += prepareUserPhotoHTML(i, usersToPrint[i].photoSrc);
  }
  elementToAppendPhotos.innerHTML = htmlElementsToOutput;
  addEventListenerToUsers();
}

function addEventListenerToUsers() {
  document
    .querySelectorAll(".feedback-content-person-list-item")
    .forEach((liItem) => {
      liItem.addEventListener("click", function () {
        const index = Number(liItem.getAttribute("data-index"));
        activeIndex = index;
        updateUserFeedbackInfo();
      });
    });
}

function getUserPhotosHTMLElement() {
  return document.querySelector(".feedback-content-person-list-items");
}

function prepareUserPhotoHTML(index, photoSrc) {
  return `<li data-index='${index}' class="feedback-content-person-list-item">
<div>
<img src="${photoSrc}" alt="">
</div>
</li>
`;
}

function updateUserFeedbackInfo() {
  const activeUser = prepareListToPrint()[activeIndex];
  document.querySelector(".feedback-content-text").textContent =
    activeUser.feedback;
  document.querySelector(".feedback-content-person-name").textContent =
    activeUser.name;
  document.querySelector(".feedback-content-person-position").textContent =
    activeUser.position;
  document.querySelector(".feedback-content-person-img-photo").src =
    activeUser.photoSrc;
  makeUserInfoActive();
}

function makeUserInfoActive() {
  const activeElement = document.querySelector(
    ".feedback-content-person-list-item.active"
  );
  if (activeElement) {
    activeElement.classList.remove("active");
  }
  document
    .querySelector(
      `[class="feedback-content-person-list-item"][data-index="${activeIndex}"]`
    )
    .classList.add("active");
}

window.addEventListener("load", function () {
  drawPictures();
  updateUserFeedbackInfo();
});

document.querySelector(".btn-left").addEventListener("click", function () {
  if (activeIndex === 0) {
    if (startPosition === 0) {
      startPosition = usersInfo.length - 1;
    } else {
      startPosition--;
    }
    drawPictures();
    updateUserFeedbackInfo();
  } else {
    activeIndex--;
    updateUserFeedbackInfo();
  }
});

document.querySelector(".btn-right").addEventListener("click", function () {
  if (startPosition === usersInfo.length - 1) {
    startPosition = 0;
    drawPictures();
    updateUserFeedbackInfo();
  }
  if (activeIndex + 1 === limitToShow) {
    startPosition++;
    drawPictures();
    updateUserFeedbackInfo();
  } else {
    activeIndex++;
    updateUserFeedbackInfo();
  }
});
