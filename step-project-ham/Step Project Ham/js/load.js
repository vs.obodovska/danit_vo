const pictureCategories = {
  Graphic: [
    "graphic-design1.jpg",
    "graphic-design2.jpg",
    "graphic-design3.jpg",
    "graphic-design4.jpg",
    "graphic-design5.jpg",
    "graphic-design6.jpg",
    "graphic-design7.jpg",
    "graphic-design8.jpg",
    "graphic-design9.jpg",
    "graphic-design10.jpg",
    "graphic-design11.jpg",
    "graphic-design12.jpg",
    "graphic-design1.jpg",
    "graphic-design2.jpg",
  ],
  Landing: [
    "landing-page1.jpg",
    "landing-page2.jpg",
    "landing-page3.jpg",
    "landing-page4.jpg",
  ],
  Web: [
    "web-design1.jpg",
    "web-design2.jpg",
    "web-design3.jpg",
    "web-design4.jpg",
    "web-design5.jpg",
    "web-design6.jpg",
    "web-design7.jpg",
  ],
  Wordpress: [
    "wordpress1.jpg",
    "wordpress2.jpg",
    "wordpress3.jpg",
    "wordpress4.jpg",
    "wordpress5.jpg",
    "wordpress6.jpg",
    "wordpress7.jpg",
    "wordpress8.jpg",
    "wordpress9.jpg",
    "wordpress10.jpg",
  ],
  All: [""],
};

function clearPictures() {
  document.querySelector(".work-tabs-img").innerHTML = "";
}

function createHTMLPictureElement(pictureInfo) {
  return `<li class="work-tab-images active" data-content="Wordpress">
<img class="work-tab-img" src="${pictureInfo.getPath()}" alt="${pictureInfo.getAlt()}"/>
<div class="work-tab-img-deploy">
  <div class="deploy-container">
    <div class="deploy-container-links">
      <a class="deploy-container-link" href="#">
        <svg
          class="deploy-container-link-icon"
          width="15"
          height="15"
          viewBox="0 0 15 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
            fill="#1FDAB5"
          />
        </svg>
      </a>
      <div class="deploy-container-cube"><a href="#"></a></div>
    </div>
    <p class="deploy-title">${pictureInfo.getDeployTitle()}</p>
    <p class="deploy-content">${pictureInfo.getDeployContent()}</p>
  </div>
</div>
</li>`;
}

function createPicturesToView(pictureCategory) {
  switch (pictureCategory) {
    case pictureCategories.Graphic:
      return createGraphicPicturesInfo();
    case pictureCategories.Web:
      return createWebPicturesInfo();
    case pictureCategories.Landing:
      return createLandingPicturesInfo();
    case pictureCategories.Wordpress:
      return createWordPressPicturesInfo();
    case pictureCategories.All:
      return createAllPicturesInfo();
  }
}

function createGraphicPicturesInfo() {
  var picturesInfo = [];
  pictureCategories.Graphic.forEach((picInfo) =>
    picturesInfo.push(createGraphicPictureInfo(picInfo))
  );
  return picturesInfo;
}

function createWebPicturesInfo() {
  var picturesInfo = [];
  pictureCategories.Web.forEach((picInfo) =>
    picturesInfo.push(createWebPictureInfo(picInfo))
  );
  return picturesInfo;
}

function createLandingPicturesInfo() {
  var picturesInfo = [];
  pictureCategories.Landing.forEach((picInfo) =>
    picturesInfo.push(createLandingPictureInfo(picInfo))
  );
  return picturesInfo;
}

function createWordPressPicturesInfo() {
  var picturesInfo = [];
  pictureCategories.Wordpress.forEach((picInfo) =>
    picturesInfo.push(createWordPressPictureInfo(picInfo))
  );
  return picturesInfo;
}

function createAllPicturesInfo() {
  return [
    ...createGraphicPicturesInfo(),
    ...createWebPicturesInfo(),
    ...createLandingPicturesInfo(),
    ...createWordPressPicturesInfo(),
  ];
}

function createWordPressPictureInfo(fileName) {
  return {
    fileName: fileName,
    getPath() {
      return "./img/work/Wordpress/" + fileName;
    },
    getDeployTitle() {
      return "Creative Design";
    },
    getDeployContent() {
      return "Wordpress";
    },
    getAlt() {
      return "Wordpress";
    },
  };
}

function createLandingPictureInfo(fileName) {
  return {
    fileName: fileName,
    getPath() {
      return "./img/work/Landing/" + fileName;
    },
    getDeployTitle() {
      return "Creative Design";
    },
    getDeployContent() {
      return "Landing Pages";
    },
    getAlt() {
      return "Landing Pages";
    },
  };
}

function createWebPictureInfo(fileName) {
  return {
    fileName: fileName,
    getPath() {
      return "./img/work/Web/" + fileName;
    },
    getDeployTitle() {
      return "Creative Design";
    },
    getDeployContent() {
      return "Web Design";
    },
    getAlt() {
      return "Web Design";
    },
  };
}
function createGraphicPictureInfo(fileName) {
  return {
    fileName: fileName,
    getPath() {
      return "./img/work/Graphic/" + fileName;
    },
    getDeployTitle() {
      return "Creative Design";
    },
    getDeployContent() {
      return "Graphic Design";
    },
    getAlt() {
      return "Graphic Design";
    },
  };
}

function drawImages(state) {
  const imageHTMLElement = document.querySelector(".work-tabs-img");
  imageHTMLElement.innerHTML = "";
  const picturesToLoad = createPicturesToView(state.images);
  const numberOfPicturesToLoad =
    picturesToLoad.length >= state.currentIndex + state.pageSize
      ? state.currentIndex + state.pageSize
      : picturesToLoad.length;
  for (let i = 0; i < numberOfPicturesToLoad; i++) {
    imageHTMLElement.innerHTML += createHTMLPictureElement(picturesToLoad[i]);
  }

  numberOfPicturesToLoad < picturesToLoad.length
    ? showLoadMoreButton()
    : hideLoadMoreButton();
}

function addButtonListener(button) {
  button.addEventListener("click", function () {
    const newState = getStateForButton(button.textContent);
    if (newState !== actualState.images) {
      removeActiveButton();
      setActiveButton(button);
      actualState.images = getStateForButton(button.textContent);
      actualState.resetIndex();
      drawImages(actualState);
    }
  });
}

function getStateForButton(buttonText) {
  switch (buttonText) {
    case "All":
      return pictureCategories.All;
    case "Graphic Design":
      return pictureCategories.Graphic;
    case "Web Design":
      return pictureCategories.Web;
    case "Landing Pages":
      return pictureCategories.Landing;
    case "Wordpress":
      return pictureCategories.Wordpress;
  }
}

const actualState = {
  currentIndex: 0,
  images: pictureCategories.All,
  pageSize: 12,
  incrementIndex() {
    this.currentIndex = this.currentIndex + this.pageSize;
  },
  resetIndex() {
    this.currentIndex = 0;
  },
};
const buttons = document.querySelectorAll(".work-tabs-title");

function removeActiveButton() {
  document.querySelector(".work-tabs-title.active").classList.remove("active");
}

function setActiveButton(button) {
  button.classList.add("active");
}

function showLoadMoreButton() {
  document.querySelector(".btn-explore.load").style.visibility = "visible";
}

function hideLoadMoreButton() {
  document.querySelector(".btn-explore.load").style.visibility = "hidden";
}

buttons.forEach((button) => addButtonListener(button));

window.addEventListener("load", function () {
  drawImages(actualState);
});

function showLoadSpinner() {
  document.querySelector(".spinner-loader").style.visibility = "visible";
}

function hideLoadSpinner() {
  document.querySelector(".spinner-loader").style.visibility = "hidden";
}
document
  .querySelector(".btn-explore.load")
  .addEventListener("click", function () {
    showLoadSpinner();
    setTimeout(function () {
      hideLoadSpinner();
      actualState.incrementIndex();
      drawImages(actualState);
    }, 1000);
  });
