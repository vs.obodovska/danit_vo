/// Теоретичне питання

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Відповідь: конструкцію try...catch можна використовувати для:
// - обробки даних, отриманих з мережі, з серверу чи з інших джерел;
// - пошуку синтаксичних чи семантичних помилок;
// - як в середині функції, так і загнавши функцию в "try";

/// Завдання

// Дано масив books.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const divElement = document.querySelector("root");

const booksToOutput = [];

books.forEach(book => {
    try{
        isValidBook(book);
         booksToOutput.push(`<li> <p>Author: ${book.author}</p> <p>name: ${book.name}</p> <p>price: ${book.price}</p></li>`)
    }
    catch(error){
        console.log(error.message)
    }
})
document.getElementById('root').insertAdjacentHTML('afterbegin', `<ul>${booksToOutput.join("")}</ul>`)

function isValidBook(book){
    isValidProperty(book.name, `name of this book ${JSON.stringify(book)} is undefined or null`);
    isValidProperty(book.author, `author of this book ${JSON.stringify(book)} is undefined or null`);
    isValidProperty(book.price, `price of this book ${JSON.stringify(book)} is undefined or null`);
}

function isValidProperty(property, msg){
     if (property == undefined || property == null)
        throw new Error(msg);
}
