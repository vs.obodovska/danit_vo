// HW6 - async-await
// Теоретичне питання

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Відповідь: асинхронність, це коли результат виконання функції доступний не одразу, а через 
// певний час після виклику асинхронної функції. Асинхронність дозволяє зробити декілька 
// швидших дій, поки не закінчиться тривала інша дія.
// Конструкція async/await дозволяє писати код, який виглядає як синхронний, але 
// використовується для вирішення асинхронних завдань та не блокує головний потік.

// Завдання

// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const ipUrl = "https://api.ipify.org/?format=json";
const ipService = "http://ip-api.com/json/";

const btnElement = document.querySelector(".btn");
const ipInfoElement = document.querySelector(".ip-info");
const dataInfoElement = document.querySelector(".ip-data");

const sendRequest = async (url, config) => {
  return await fetch(url, {
    method: "GET",
    ...config,
  }).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      return new Error("Чтото пошло не так");
    }
  });
};

class Data {
  constructor({ continent, country, region, city, district }) {
    this.continent = continent;
    this.country = country;
    this.region = region;
    this.city = city;
    this.district = district;
  }

  renderInfo() {
    this.info = document.createElement('div');
    this.info.classList.add("card");
    this.info.style.display = "block";
    this.info.innerHTML = `
        <p class="continent">Continent: ${this.continent}</p>
        <p class="country">Country: ${this.country}</p>
        <p class="region">Region: ${this.region}</p>
        <p class="city">City: ${this.city}</p>
        <p class="district">District: ${this.district}</p>
    `;
    dataInfoElement.append(this.info);
  }
}

const getIp = () => sendRequest(ipUrl);
const getIpData = (ip) =>
  sendRequest(
    `${ipService}${ip}?fields=continent,country,region,city,district`
  );

btnElement.addEventListener("click", async () => {
  const dataIp = await getIp().then((data) => {
    return data.ip;
  });
  ipInfoElement.style.display = "block";
  ipInfoElement.innerHTML = `Your IP: ${dataIp}`;

  await getIpData(dataIp).then((data) => {
    dataInfoElement.style.display = "block";
    return new Data(data).renderInfo();
  });
});
   
