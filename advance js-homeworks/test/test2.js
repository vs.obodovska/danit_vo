
const baseURL = "https://ajax.test-danit.com/api/json/";
const getUsers = () => sendRequest("users");
const getPosts = (id) => sendRequest(`posts?userId=${id}`);

const sendRequest = async (entity, method = "GET", config) => {
  return await fetch(`${baseURL}${entity}`, {
    method,
    ...config,
  }).then((response) => {
    if (response.ok) {
      if (method === "GET" || method === "POST" || method === "PUT") {
        return response.json();
      }
      return response;
    }
  });
};

class User {
  constructor(id, userName, name, email) {
    this.id = id;
    this.userName = userName;
    this.name = name;
    this.email = email;
  }
}

class Post {
  constructor(user, title, body) {
    this.user = user;
    this.title = title;
    this.body = body;
  }
}

async function fetchAllPosts() {
  return await getUsers()
    .then((users) => {
      return users.map((user) => {
        return new User(user.id, user.username, user.name, user.email);
      });
    })
    .then((users) => {
      return Promise.all(
        users.map(async (user) => {
          return await fetchUserPosts(user);
        })
      );
    });
}

const allUsersPosts = fetchAllPosts();
allUsersPosts.then((posts) => {
  posts.forEach((postsByAuthor) => {
    postsByAuthor.forEach((post) => {
      renderPost(post);
    });
  });
});

async function fetchUserPosts(user) {
  return await getPosts(user.id).then((posts) => {
    return posts.map((post) => {
      return new Post(user, post.title, post.body);
    });
  });
}

function renderPost(post) {
  document.body.insertAdjacentHTML(
    "afterbegin",
    `
  <div class="user-wrap-${post.user.id}" style="position: relative">
      <div class="card-navigation">
        <div class="edit" >
          edit
        </div>
        <div class="del">del</div>
      </div>
      <div class="user-details" style="font-size: 16px; font-weight: bold">
        <p class="user-name">${post.user.name}</p>
        <a class="user-link" href="#">@${post.user.userName}</a>
      </div>
      <div class="user-email">
      <a href="mailto:${post.user.email}">${post.user.email}</a></div>
      <div class="post-wrap">
      <div class="post-title">${post.title}</div>
      <div class="post-body">${post.body}</div>
      </div>`
  );
}
