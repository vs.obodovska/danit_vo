
const baseURL = "https://ajax.test-danit.com/api/json/";

const getUsers = () => sendRequest("users");
const getPosts = (id) => sendRequest(`users/${id}/posts`);
const sendRequest = async (entity, method = "GET", config) => {
    return await fetch(`${baseURL}${entity}`, {
        method,
        ...config,
    }).then((response) => {
        if (response.ok) {
            if (method === "GET" || method === "POST" || method === "PUT") {
                return response.json();
            }
            return response;
        }
    });
};

class UserCard {
    constructor(id, userName, name, email) {
        this.id = id;
        this.userName = userName;
        this.name = name;
        this.email = email;
        this.posts = null
    }

}

class Post {
    constructor(title, body) {
        this.title = title;
        this.body = body;
    }
}

async function fetchUsers() {
    return await getUsers()
        .then((users) => {
            return users.map(user => {
                return new UserCard(user.id, user.username, user.name, user.email);
            });
        })
        .then((users) => {
            return Promise.all(users.map(async user => {
                const userPosts = await fetchUserPosts(user.id);
                user.posts = userPosts;
                return {...user};
            }));
        });
}

const allUserCards = fetchUsers()
allUserCards.then(allUserCards => {
    allUserCards.forEach(userCard=>renderUser(userCard))
})


async function fetchUserPosts(id) {
    return await getPosts(id).then((posts) => {
        return posts.map((post) => {
            return new Post(post.title, post.body);
        });
    });
}

function renderUser(userCard) {
    document.body.insertAdjacentHTML(
        "afterbegin",
        `
  <div class="user-wrap-${userCard.id}" style="position: relative">
      <div class="card-navigation">
        <div class="edit" >
          edit
        </div>
        <div class="del">del</div>
      </div>
      <div class="user-details" style="font-size: 16px; font-weight: bold">
        <p class="user-name">${userCard.name}</p>
        <a class="user-link" href="#">@${userCard.userName}</a>
      </div>
      <div class="user-email">
      <a href="mailto:${userCard.email}">${userCard.email}</a></div>
    </div>`
    );
    const userElement = document.querySelector(`.user-wrap-${userCard.id}`);
    userCard.posts.forEach((post) => {
        userElement.insertAdjacentHTML(
            "beforeend",
            `<div class="post-wrap">
      <div class="post-title">${post.title}</div>
      <div class="post-body">${post.body}</div>
      </div>`
        );
    });
}

