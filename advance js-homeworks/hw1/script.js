// HW1 - ES6-classes

// Теоретичне питання //

// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Відповідь: наприклад, ми маємо якийсь загальний об'єкт з певними загальними властивостями, 
// і хочемо в інший спеціальний об'єкт додатково вкласти властивості загального об'єкту. В такому випадку буде використання 
//specialObj.__proto__ = generalObj і вже specialObj матиме/ прочитаємо властивості, які були і в generalObj - успадковані
// Можна і створити такий спеціальний об'єкт на основі наслідування загального через Object.create, через конструктор та клас.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Відповідь: щоб перш ніж записувати в нащадку свої властивості треба виконати батьківський конструктор, і лише після того ініціалізувати свої поля класу,
// або ж прийняти батьківський метод від свого прототипу, якщо є використання this.

// Завдання //

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    
    constructor() {
        this._name = null; 
        this._age = null; 
        this._salary = null
      }
    
      set name(newName) {
        if (newName.length < 1 ) {
          console.log('Name should not be empty ');
          return
        }
        this._name = newName
      }
      get name() {
        return this._name;
      }

    
      set age(newAge) {
        if (newAge < 14) {
            console.log('Wrong age ' + newAge)
            return
        }
        this._age = newAge
      }
      get age() {
        return this._age;
      }

      set salary(newSalary) {
        if (newSalary < 1) {
            console.log('Wrong salary ' + newSalary);
            return; 
        }
        this._salary = newSalary;
      }
      get salary() {
        return this._salary;
      }
}


class Programmer extends Employee {
    constructor () {
    super ()
    this._lang = null;
    }

    set lang (newLang) {
        const languages = ["English", "Ukrainian", "Polish"];
            if (languages.includes(newLang)) {
                this._lang = newLang
            } else {
                console.log('Wrong language ' + newLang)
            }
        }
    get lang() {
            return this._lang;
        }

    
    set salary(newSalary) {
        super.salary  = newSalary
    }

    get salary() {
    return super.salary * 3;
        }
}

let programmer = new Programmer();
programmer.name = "";
programmer.age = 32;
programmer.salary = 110;
programmer.lang = 'Ukrainian';
console.log('prormammer', programmer);

let programmer2 = new Programmer();
programmer2.name = "Oleg";
programmer2.age = 13;
programmer2.salary = 100;
programmer2.lang = 'English';
console.log('prormammer2', programmer2);

let programmer3 = new Programmer();
programmer3.name = "Alina";
programmer3.age = 25;
programmer3.salary = 0; 
programmer3.lang = 'Polish';
console.log('prormammer3', programmer3);

let programmer4 = new Programmer();
programmer4.name = "Olga";
programmer4.age = 25;
programmer4.salary = 500; 
programmer4.lang = 'Russian';
console.log('prormammer4', programmer4);

console.log('prormammer4 salary get', programmer4.salary);