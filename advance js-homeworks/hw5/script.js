// HW5 - Twitter
// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.

const url = "https://ajax.test-danit.com/api/json/";

const containerElement = document.querySelector(".container");
const userCardElement = document.querySelector(".user-card");

//1. Отримати список всіх користувачів
//2. Отримати список всіх публікацій такого користувача
//3. завантажити всіх користувачів та їх публікації і відобразити на сторінці
//4. Прив'язка іде до публікації юзера.
//5. DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}
//6. Js видалити картку

const sendRequest = async (entity, method = "GET", config) => {
  return await fetch(`${url}${entity}`, {
    method,
    ...config,
  }).then((response) => {
    if (response.ok) {
      if (method === "GET" || method === "POST" || method === "PUT") {
        return response.json();
      }
      return response;
    }
  });
};

const getUsers = () => sendRequest("users");
const getPosts = (id) => sendRequest(`users/${id}/posts`);
const deletePost = (postId) => sendRequest(`posts/${postId}`, "DELETE");
setTimeout(() => {
getUsers().then((users) => {
  users.forEach(({ name, username, email, id }) => {
    getPosts(id).then((posts) => {
      posts.forEach(({ title, body }) => {
        userCardElement.insertAdjacentHTML(
          "afterbegin",
          `<div class="user-wrap" data-userId="${id}" style="position: relative">
                <div class="card-navigation">
                    <div class="edit">edit</div>
                    <div class="del">del</div>
                </div>
          <div class="user-details" style="font-size: 16px; font-weight: bold; display: grid">
                <p class="user-name">${name}</p>
                <a class="user-link" href="#">@${username}</a>
                <a class="user-email" href="mailto:${email}">${email}</a>
          </div>
          <div class="post-wrap">
                <div class="post-title">${title}</div>
                <div class="post-body">${body}</div>
            </div>
        </div>`
        );
        const deleteButton = document.querySelector(".del");
        deleteButton.addEventListener("click", () => {
          deletePost(id).then((response) => console.log(response));
          const postItem = document.querySelector(".user-wrap");
          postItem.remove()
          alert('Deleted Post success', 'success');
        });
      });
    });
  });
  document.querySelector('.loader').remove();
});
}, 3000);