/// Теоретичне питання

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Відповідь:
// AJAX - це технологія, яка дозволяє підвантажувати ряд даних на веб-сторінці без перевантаження самої сторінки. Це зменшує кількість запитів до сервера, а також об’єм підвантажуваних даних. Приклади використання: авторизація, додання коментарів, відправка повідомлень.
// У JavaScript отримати дані AJAX можливо у вигляді відповіді формату XML чи JSON.
// AJAX корисний тим, що така технологія дозволяє оновлювати запит користувача без оновлення сторінки, одразу генерувати та отримувати дані з сервера після завантаження сторінки, надсилати дані у фоновому режимі на сервер. 


/// Завдання

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

const API = "https://ajax.test-danit.com/api/swapi/films";

const filmsContainer = document.querySelector("#films");

const render = (url) => {
  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const filmCharactersToFetch = new Map();
      data.forEach(({ id, episodeId, name, openingCrawl, characters }) => {
        listFilmInfo(id, episodeId, name, openingCrawl);
        filmCharactersToFetch.set(id, characters);
      });
      return filmCharactersToFetch;
    })
    .then((characters) => {
      const mapPromises = new Map();
      characters.forEach((character, id) => {
        const promises = [];
        character.forEach((URL) => {
          promises.push(
            fetch(URL)
              .then((response) => {
                return response.json();
              })
              .then((response) => {
                listFilmCharacterInfo(id, response.name);
              })
          );
        });
       mapPromises.set(id, promises);
      });
      return mapPromises;
    })
    .then(mapPromises=>{
        mapPromises.forEach((promises, id)=>{
            Promise.all(promises).finally(()=>removeLoader(id));
        })
    })
    .catch((error) => console.log(error.message));
};

function listFilmInfo(id, episodeId, name, openingCrawl) {
filmsContainer.insertAdjacentHTML('beforeend', ` 
<div id= "${id}" class="film-item">
<p class="film-name"> ${name} </p>
<p class="film-episode"> Episode: ${episodeId} </p>
<p class="film-info"> <span> About: </span> ${openingCrawl} </p>
<div class= "characters">
<p class= "characters-title">Actors:</p>
<ul id ="characters-${id}" class="characters-list"></ul></div>
<div id="loader-${id}" class="loader"</div></div>
</div>`)
}

function removeLoader(id) {
  document.querySelector(`#loader-${id}`).remove();
}

function listFilmCharacterInfo(id, character) {
  const filmCharacterElement = document.querySelector(`#characters-${id}`);
  filmCharacterElement.innerHTML += `
    <li>${character} </li>`;
}

render(API);
