import './App.scss';
import ProductList from "./components/ProductList/ProductList.jsx";
import Header from "./components/Header/Header.jsx";
import {Component} from "react";

class App extends Component {
  constructor() {
    super();

    const initialStateCart = localStorage.getItem('cart');
    const initialStateFavs = localStorage.getItem('favs');
    this.state = {
      cart: initialStateCart ? JSON.parse(initialStateCart) : [],
      favourites: initialStateFavs ? JSON.parse(initialStateFavs) : [],
    }

  }


  addToFavourites = (code) => {
    this.setState({
      ...this.state,
      favourites: [...this.state.favourites, code]
    })

    localStorage.setItem('favs', JSON.stringify([...this.state.favourites, code]))
  }
  removeFromFavourites = (code) => {
    this.setState({
      ...this.state,
      favourites: this.state.favourites.filter((fav) => fav !== code)
    })
    localStorage.setItem('favs', JSON.stringify(this.state.favourites.filter((fav) => fav !== code)))

  }

  addToCart = (code) => {
    this.setState({
      ...this.state,
      cart: [...this.state.cart, code]
    })
    localStorage.setItem('cart', JSON.stringify([...this.state.cart, code]))

  }

  render() {
    return (
      <div className="App">
        <Header favourites={this.state.favourites} cart={this.state.cart}/>
        <ProductList favourites={this.state.favourites}
                     cart={this.state.cart}
                     addToFavourites={this.addToFavourites}
                     removeFromFavourites={this.removeFromFavourites}
                     addToCart={this.addToCart}
        />
      </div>
    );
  }
}

export default App;
