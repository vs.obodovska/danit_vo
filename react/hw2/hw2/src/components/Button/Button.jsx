import {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

class Button extends Component {

    render() {
        const {textBtn, backgroundColor, onClick, type} = this.props;
        
        return (
            <button className="openModalBtn" type={type} onClick={onClick} style={{backgroundColor}}>{textBtn}</button>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    textBtn: PropTypes.string,
  };
Button.defaultProps = {
    type: 'button',
}

export default Button;