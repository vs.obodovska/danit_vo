import React, {Component} from 'react';
import './ProductItem.scss'
import Modal from '../Modal'

class ProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
    }
  }

  showModal = () => {
    this.setState({
      isModalOpen: true,
    })
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
    })
  }

  addToCart = () => {
    if (this.props.isInCart) {
      this.props.addToCart(this.props.item.code)
    } else {
      this.props.addToCart(this.props.item.code)
    }
    this.closeModal();
  }

  addToFav = () => {
    if (this.props.isInFavourites) {
      this.props.removeFromFavourites(this.props.item.code)
    } else {
      this.props.addToFavourites(this.props.item.code)
    }
  }

  render() {

    const actions =
      <>
        <button onClick={this.addToCart}>
          CONFIRM
        </button>
        <button onClick={this.closeModal}>
          CANCEL
        </button>
      </>;


    const {name, url, price, code, color} = this.props.item;

    return (
      <>
        <div className="products__product-item">
          <h3 className="products__product-item-name">{name}</h3>
          <div className="products__product-item-img">
            <img className="product-img" src={url} alt={name}/>
          </div>
          <p className="products__product-item-price">{price} UAH</p>
          <p className="products__product-item-code">Code: {code}</p>
          <p className="products__product-item-color">Color: {color}</p>
          <div className="products__product-item-actions">
            <button className="products__btn"
                    onClick={this.showModal}>{this.props.isInCart ? 'Add more' : 'Add to cart'}</button>
            <button className="fav-img" onClick={this.addToFav}><img
              src={this.props.isInFavourites ? "/img/favourite-check.png" : "/img/favourite.png"} alt=""/></button>
          </div>

        </div>

        {this.state.isModalOpen && <Modal header="Do you want to add this item to cart?"
                                          text="Please choose the relevant buttun to CONFIRM or CANCEL"
                                          actions={actions}/>}
      </>

    );
  }
}

export default ProductItem;
