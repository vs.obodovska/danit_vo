import React, {Component} from 'react';
import './ProductList.scss'
import ProductItem from "../ProductItem/ProductItem.jsx";

class ProductList extends Component {

  constructor() {
    super();

    this.state = {
      products: [],
    }
  }


  componentDidMount() {
    const url = process.env.PUBLIC_URL + "/product.json";
      fetch(url)
      .then(res => res.json())
      .then(products => {
        this.setState({
          products: products,
        })
      })
  }

  render() {
    return (
      <div className="product-list">
        {
          this.state.products.map((product) => {
            const isInFavourites = this.props.favourites.includes(product.code);
            const isInCart = this.props.cart.includes(product.code);
            
            return <ProductItem isInFavourites={isInFavourites}
                                isInCart={isInCart}
                                item={product}
                                key={product.code}
                                addToCart={this.props.addToCart}
                                addToFavourites={this.props.addToFavourites}
                                removeFromFavourites={this.props.removeFromFavourites}

            />
          })
        }
      </div>
    );
  }
}

export default ProductList;