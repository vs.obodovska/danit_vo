import { Component } from "react";

import './Header.scss';

class Header extends Component {
        render() {
          return (
            <header className="header">
              <div className="header__logo">
                <img src="/img/logo.png" alt="" />
                <h1 className="header__title">Gel polish shop</h1>
              </div>
              <div className="shop-info">
                <a className="favorites" href="#">
                  <img src="/img/favourite-head.png" alt="fav" />
                  {this.props.favourites.length}
                </a>
                <a href="#" className="cart">
                  <img src="/img/cart.png" alt="cart" />
                  {this.props.cart.length}
                </a>
              </div>
            </header>
          );
        }
      }

export default Header;
