import React, {Component} from 'react';
import './Modal.scss';

class Modal extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      const {header, text, closeButton, actions, closeModal} = this.props;
  
      return (
        <div className="modal open">
          <div className="modal__backdrop" onClick={closeModal}></div>
          <div className="modal__content">
            <div className="modal__title">{header}</div>
            {closeButton && (
              <button
                onClick={() => closeModal()}
                className="modal__close-button">
                x
              </button>
            )}
            <div className="modal__text">{text}</div>
            <div className="modal__button-actions">{actions}</div>
          </div>
        </div>
      );
    }
  }

export default Modal;
