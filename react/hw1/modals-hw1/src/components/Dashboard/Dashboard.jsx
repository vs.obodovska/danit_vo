import React, {Component} from 'react';
import Button from '../Button/Button';
import Modal from '../Modal';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOne: false,
      modalTwo: false,
    };
    this.showModalOne = this.showModalOne.bind(this);
    this.showModalTwo = this.showModalTwo.bind(this);
    this.closeModals = this.closeModals.bind(this);
  }
  showModalOne() {
    this.setState({modalOne: true});
  }

  showModalTwo() {
    this.setState({modalTwo: true});
  }
  closeModals() {
    this.setState({
      modalOne: false,
      modalTwo: false,
    })
  }

  render() {
    const actionsModalOne =
      <>
        <Button textBtn="Confirm" backgroundColor="rgba(14, 11, 12, 0.3);"
        />
        <Button textBtn="Cancel" backgroundColor="rgba(14, 11, 12, 0.3);"/>
      </>

    const actionsModalTwo = <Button textBtn="Show more" backgroundColor="#4169E1"/>;

    return (
    <div className='page-wrapper'>
    <h1 className='text'>Open one of the below Modals</h1>
            <div className='buttons'>
              <Button
              textBtn="Open first modal"
              backgroundColor="#ffc800"
              onClick={this.showModalOne}
              />
              <Button
              textBtn="Open second modal"
              backgroundColor="#002aff"
              onClick={this.showModalTwo}
              />
              </div> 
     
    {this.state.modalOne && (
          <Modal
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action. are you sure you want to delete it?"
            closeButton="true"
            actions={actionsModalOne}
            closeModal={this.closeModals}
          />
        )}

        {this.state.modalTwo && (
          <Modal
            header="Do you want to get updates?"
            text="Our store has weekly sales on some goods and you are able to get updates about it. If you want to know more about it - hoose the button below."
            closeButton="true"
            actions={actionsModalTwo}
            closeModal={this.closeModals}
          />
        )}
        </div>
    );
  }
}

export default Dashboard