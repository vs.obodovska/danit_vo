import React, {Component} from 'react';

import './Button.scss'

class Button extends Component {

    render() {
        const {textBtn, backgroundColor, onClick, type} = this.props;
        
        return (
            <button className="openModalBtn" type={type} onClick={onClick} style={{backgroundColor}}>{textBtn}</button>
        )
    }
}

Button.defaultProps = {
    type: 'button',
}

export default Button;